import React from 'react'

import FIXTURES from './RnRData.json'

export function useWebsocket({ url }: { url: string }) {
  const ws = React.useRef<WebSocket>(new WebSocket(url))

  React.useEffect(() => {
  }, [])
}

export interface Rating {
  id: string,
  href: string,
  eqyYear: number,
  period: string,
  financialDate: string,
  reportingPeriod: string,
  fhr: number,
  chs: number,
  probabilityDefault: number,
  termPd: {
    year2: number,
    year3: number,
    year4: number,
    year5: number,
    year6: number,
    year7: number,
    year8: number,
    year9: number
    year10: number
  },
  delta: number,
  simulatedFhr: number,
  simulatedFhrDelta: number,
  operatingProfitability: number,
  netProfitability: number,
  capitalStructureEfficiency: number,
  costStructureEfficieny: number,
  leverage: string,
  liquidity: string,
  earningsPerformance: string,
}

export interface RnrMessage {
  id: string,
  // [key: string]: any
  name: string,
  country: string,
  ticker: string,
  exchange: string,
  sector: string,
  href: string,
  financials: string,
  periods: string,
  ratings: string,
  latestRatings: Rating[],
  reportLinks: {
    financialDialog: string,
    fhr: string,
  },
  address: string,
  reports: any[],
  sectorAverages: any,
  industryClassificationSystem: any,
  industryClassificationCode: any,
  dataScale: string,
  currency: string,
  dataConfidential: string,
  externalSourceId: any
}

export function RnrPlugin(props: any) {
  // const socket = useWebsocket({ url })

  const [data, setData] = React.useState<RnrMessage>(FIXTURES)

  const {
    name,
    country,
    ticker,
    exchange,
    sector,
    latestRatings,
    reportLinks,
    address,
    sectorAverages,
  } = data

  return (
    <div>
      <div style={{ 
        display: "flex",
        justifyContent: "space-around"
      }}>
        <div style={{ textAlign: "left" }}>
          <h3>RapidRatings Financial Health Analysis</h3>
          <h4>{name}</h4>
          <p>{country}</p>
          <p>{ticker}</p>
          <p>{address}</p>
          {latestRatings.map((rating: Rating) => {
            const {
              eqyYear,
              period,
              financialDate,
              reportingPeriod,
              fhr,
              chs,
              probabilityDefault,
              termPd,
              delta,
              simulatedFhr,
              simulatedFhrDelta,
              operatingProfitability
            } = rating

            return (
              <div>
                <div>
                  <p>{eqyYear} - {period} - {financialDate} - {reportingPeriod}</p>
                  <p>FHR - {fhr}, CHS - {chs}, Probability Default - {probabilityDefault}</p>
                  <p>{delta} - {simulatedFhr} - {simulatedFhrDelta} - {operatingProfitability}</p>
                </div>
              </div>
            )
          })}
        </div>
        <div></div>
      </div>
      <hr/>
      <div>
        <button>Download Financial Dialog</button>
        <button>Download FHR Report</button>
      </div>
    </div>
  )
}
