import React from 'react';
import './App.css';

import { EcovadisPlugin } from './EcovadisPlugin'
import { RnrPlugin } from './RnRPlugin'

type PluginContainerProps = {
  Component: (props: any) => JSX.Element
}

function PluginContainer(props: PluginContainerProps) {
  const componentProps = {
    duns: "294036723"
  }

  return (
    <div style={{ 
      border: "1px solid #eee", 
      margin: "1rem 10rem 1rem 10rem", 
      borderRadius: "3px", 
      boxShadow: "1px 1px 2px 2px #ddd"
    }}>
      <props.Component {...componentProps} />
    </div>
  )
}

const App: React.FC = () => {
  return (
    <div className="App">
      <header><h1>TYS Ecovadis Plugin Demo</h1></header>
      <PluginContainer 
        Component={EcovadisPlugin}
      />
      <PluginContainer 
        Component={RnrPlugin} 
      />
    </div>
  );
}

export default App;
